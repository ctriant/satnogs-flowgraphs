options:
  parameters:
    author: Manolis Surligas (surligas@gmail.com)
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: FSK 9600 Decoder for the Reaktor-Hello-World satellite
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: satnogs_reaktor_hello_world_fsk9600_decoder
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: satnogs_reaktor_hello_world_fsk9600_decoder
    window_size: 2*1080,1080
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 20]
    rotation: 0
    state: enabled

blocks:
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1264, 28.0]
    rotation: 0
    state: enabled
- name: decimation
  id: variable
  parameters:
    comment: ''
    value: max(4,satnogs.find_decimation(baudrate, 2, audio_samp_rate))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1056, 28.0]
    rotation: 0
    state: true
- name: variable_ieee802_15_4_variant_decoder_0
  id: variable_ieee802_15_4_variant_decoder
  parameters:
    comment: ''
    crc: satnogs.crc.CRC16_IBM
    frame_len: '256'
    preamble: '[0x55, 0x55, 0x55, 0x55, 0x55]'
    preamble_thrsh: '2'
    sync_thrsh: '1'
    sync_word: '[0x35, 0x2E, 0x35, 0x2E]'
    var_len: 'True'
    whitening: variable_whitening_0
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1064, 900.0]
    rotation: 0
    state: enabled
- name: variable_whitening_0
  id: variable_whitening
  parameters:
    comment: ''
    mask: '0x21'
    order: '8'
    seed: '0x1FF'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [920, 932.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.2'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1416, 468.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '0.9'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 348.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [432, 532.0]
    rotation: 0
    state: enabled
- name: antenna
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 28.0]
    rotation: 0
    state: enabled
- name: baudrate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '9600.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1168, 28.0]
    rotation: 0
    state: enabled
- name: blocks_delay_0
  id: blocks_delay
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    delay: 1024//2
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [448, 452.0]
    rotation: 0
    state: enabled
- name: blocks_moving_average_xx_0
  id: blocks_moving_average_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    max_iter: '4096'
    maxoutbuf: '0'
    minoutbuf: '0'
    scale: 1.0/1024.0
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [584, 508.0]
    rotation: 0
    state: enabled
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [952, 456.0]
    rotation: 0
    state: enabled
- name: blocks_vco_c_0
  id: blocks_vco_c
  parameters:
    affinity: ''
    alias: ''
    amplitude: '1.0'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    sensitivity: -baudrate*decimation
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [760, 516.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [952, 28.0]
    rotation: 0
    state: enabled
- name: dc_blocker_xx_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1424, 668.0]
    rotation: 180
    state: enabled
- name: dc_blocker_xx_0_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [904, 340.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/.satnogs/data/data"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 876.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [352, 28.0]
    rotation: 0
    state: enabled
- name: digital_binary_slicer_fb_0
  id: digital_binary_slicer_fb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1104, 680.0]
    rotation: 180
    state: enabled
- name: digital_clock_recovery_mm_xx_0
  id: digital_clock_recovery_mm_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain_mu: 0.5/8.0
    gain_omega: 2 * math.pi / 100
    maxoutbuf: '0'
    minoutbuf: '0'
    mu: '0.5'
    omega: '2'
    omega_relative_limit: '0.01'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1224, 644.0]
    rotation: 180
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [168, 876.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [264, 796.0]
    rotation: 0
    state: enabled
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"test.wav"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [152, 796.0]
    rotation: 180
    state: enabled
- name: gain
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [880, 28.0]
    rotation: 0
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [0, 172.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/iq.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 796.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 28.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: 0.625 * baudrate
    decim: decimation // 2
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 8.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1072, 420.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: "Perform a relaxed filter to increase \nthe performance of the auto frequency\n\
      correction"
    cutoff_freq: baudrate*1.25
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 2.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [264, 484.0]
    rotation: 0
    state: enabled
- name: pfb_arb_resampler_xxx_1
  id: pfb_arb_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    atten: '80'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    nfilts: '32'
    rrate: audio_samp_rate / (baudrate*decimation)
    samp_delay: '0'
    taps: ''
    type: fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [672, 324.0]
    rotation: 0
    state: true
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 892.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [640, 28.0]
    rotation: 0
    state: enabled
- name: samp_rate_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [488, 28.0]
    rotation: 0
    state: enabled
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '1'
    fine_correction: '0'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: baudrate*decimation
    samp_rate: samp_rate_rx
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 140.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ieee802_15_4_variant_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [888, 680.0]
    rotation: 180
    state: enabled
- name: satnogs_frame_file_sink_0_1_0
  id: satnogs_frame_file_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    output_type: '0'
    prefix_name: decoded_data_file_path
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 660.0]
    rotation: 180
    state: enabled
- name: satnogs_iq_sink_0
  id: satnogs_iq_sink
  parameters:
    activate: enable_iq_dump
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    filename: iq_file_path
    scale: '16768'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [24, 508.0]
    rotation: 180
    state: enabled
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 692.0]
    rotation: 180
    state: true
- name: satnogs_ogg_encoder_0
  id: satnogs_ogg_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filename: file_path
    quality: '1.0'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1056, 332.0]
    rotation: 0
    state: enabled
- name: satnogs_tcp_rigctl_msg_source_0
  id: satnogs_tcp_rigctl_msg_source
  parameters:
    addr: '"127.0.0.1"'
    affinity: ''
    alias: ''
    comment: ''
    interval: int(1000.0/doppler_correction_per_sec) + 1
    maxoutbuf: '0'
    minoutbuf: '0'
    mode: 'False'
    mtu: '1500'
    port: rigctl_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [224, 252.0]
    rotation: 0
    state: enabled
- name: satnogs_udp_msg_sink_0_0
  id: satnogs_udp_msg_sink
  parameters:
    addr: udp_IP
    affinity: ''
    alias: ''
    comment: ''
    mtu: '1500'
    port: udp_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 724.0]
    rotation: 180
    state: enabled
- name: satnogs_waterfall_sink_0
  id: satnogs_waterfall_sink
  parameters:
    affinity: ''
    alias: ''
    center_freq: rx_freq
    comment: ''
    fft_size: '1024'
    filename: waterfall_file_path
    mode: '1'
    rps: '10'
    samp_rate: baudrate*decimation
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [24, 316.0]
    rotation: 180
    state: enabled
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"driver=invalid"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [224, 28.0]
    rotation: 0
    state: enabled
- name: soapy_source_0
  id: soapy_source
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: antenna
    ant1: RX2
    args: dev_args
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: soapy_rx_device
    devname: custom
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    manual_gain0: 'False'
    manual_gain1: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    mix_gain1: '10'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: gain
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rfgr_gain: '9'
    samp_rate: samp_rate_rx
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    tia_gain0: '0'
    tia_gain1: '0'
    tuner_gain0: '10'
    tuner_gain1: '10'
    type: fc32
    vga_gain0: '10'
    vga_gain1: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [224, 116.0]
    rotation: 0
    state: true
- name: udp_IP
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"127.0.0.1"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 796.0]
    rotation: 0
    state: enabled
- name: udp_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '16887'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [504, 892.0]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [784, 172.0]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [24, 452.0]
    rotation: 0
    state: true
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/waterfall.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 796.0]
    rotation: 0
    state: enabled

connections:
- [analog_quadrature_demod_cf_0_0, '0', dc_blocker_xx_0, '0']
- [analog_quadrature_demod_cf_0_0_0, '0', pfb_arb_resampler_xxx_1, '0']
- [analog_quadrature_demod_cf_0_0_0_0, '0', blocks_moving_average_xx_0, '0']
- [blocks_delay_0, '0', blocks_multiply_xx_0, '0']
- [blocks_moving_average_xx_0, '0', blocks_vco_c_0, '0']
- [blocks_multiply_xx_0, '0', low_pass_filter_0, '0']
- [blocks_vco_c_0, '0', blocks_multiply_xx_0, '1']
- [dc_blocker_xx_0, '0', digital_clock_recovery_mm_xx_0, '0']
- [dc_blocker_xx_0_0, '0', satnogs_ogg_encoder_0, '0']
- [digital_binary_slicer_fb_0, '0', satnogs_frame_decoder_0_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', digital_binary_slicer_fb_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0_0, '0']
- [low_pass_filter_0_0, '0', analog_quadrature_demod_cf_0_0_0_0, '0']
- [pfb_arb_resampler_xxx_1, '0', dc_blocker_xx_0_0, '0']
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0_0, out, satnogs_json_converter_0, in]
- [satnogs_json_converter_0, out, satnogs_frame_file_sink_0_1_0, frame]
- [satnogs_json_converter_0, out, satnogs_udp_msg_sink_0_0, in]
- [satnogs_tcp_rigctl_msg_source_0, freq, satnogs_doppler_compensation_0, doppler]
- [soapy_source_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', analog_quadrature_demod_cf_0_0_0, '0']
- [virtual_source_0, '0', blocks_delay_0, '0']
- [virtual_source_0, '0', low_pass_filter_0_0, '0']
- [virtual_source_0, '0', satnogs_iq_sink_0, '0']
- [virtual_source_0, '0', satnogs_waterfall_sink_0, '0']

metadata:
  file_format: 1
