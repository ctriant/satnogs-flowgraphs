options:
  parameters:
    author: Thanos Giolias (agiolias@csd.uoc.gr), Nikos Karamolegos (karamolegkos.n@gmail.com),
      Manolis Surligas (surligas@gmail.com)
    category: Custom
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: A DUV Decoder for the AMSAT FOX satellites
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: satnogs_amsat_fox_duv_decoder
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: AMSAT FOX DUV Decoder
    window_size: 3000, 3000
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1056, 28.0]
    rotation: 0
    state: enabled
- name: deviation
  id: variable
  parameters:
    comment: ''
    value: '5000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [616, 836.0]
    rotation: 0
    state: enabled
- name: max_modulation_freq
  id: variable
  parameters:
    comment: ''
    value: '3000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [616, 772.0]
    rotation: 0
    state: enabled
- name: variable_amsat_duv_decoder_0
  id: variable_amsat_duv_decoder
  parameters:
    comment: ''
    control_symbol: '0011111010'
    frame_len: '96'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1368, 740.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 420.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.2'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 572.0]
    rotation: 0
    state: enabled
- name: antenna
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [792, 28.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [952, 28.0]
    rotation: 0
    state: enabled
- name: dc_blocker_xx_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [896, 564.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/.satnogs/data/data"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 852.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [336, 28.0]
    rotation: 0
    state: enabled
- name: digital_binary_slicer_fb_0_0
  id: digital_binary_slicer_fb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1440, 576.0]
    rotation: 0
    state: enabled
- name: digital_clock_recovery_mm_xx_0_0_0
  id: digital_clock_recovery_mm_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain_mu: '0.175'
    gain_omega: 0.25*0.175*0.175
    maxoutbuf: '0'
    minoutbuf: '0'
    mu: '0.5'
    omega: (audio_samp_rate/100.0) / 200
    omega_relative_limit: '0.005'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1248, 540.0]
    rotation: 0
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [176, 852.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [272, 772.0]
    rotation: 0
    state: enabled
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"test.wav"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [160, 772.0]
    rotation: 180
    state: enabled
- name: gain
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [872, 28.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/iq.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 772.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [712, 28.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: 'Using the Carson bandwidth rule for filter width:

      width = 2*(deviation + max_modulation_freq).

      For the majority of FM transmissions we can expect

      max_modulation_freq = 3000'
    cutoff_freq: deviation+max_modulation_freq
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: audio_samp_rate
    type: fir_filter_ccf
    width: '3000'
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [320, 524.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_1
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: '195'
    decim: '100'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: audio_samp_rate
    type: fir_filter_fff
    width: '10'
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [728, 524.0]
    rotation: 0
    state: enabled
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 852.0]
    rotation: 0
    state: enabled
- name: root_raised_cosine_filter_0
  id: root_raised_cosine_filter
  parameters:
    affinity: ''
    alias: ''
    alpha: '0.5'
    comment: ''
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    ntaps: '512'
    samp_rate: '1'
    sym_rate: '2.4'
    type: fir_filter_fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 532.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [632, 28.0]
    rotation: 0
    state: enabled
- name: samp_rate_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 28.0]
    rotation: 0
    state: enabled
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '1'
    fine_correction: '0'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: audio_samp_rate
    samp_rate: samp_rate_rx
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [472, 156.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_amsat_duv_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1368, 672.0]
    rotation: 180
    state: enabled
- name: satnogs_frame_file_sink_0_1_0
  id: satnogs_frame_file_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    output_type: '0'
    prefix_name: decoded_data_file_path
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1000, 676.0]
    rotation: 180
    state: enabled
- name: satnogs_iq_sink_0
  id: satnogs_iq_sink
  parameters:
    activate: enable_iq_dump
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    filename: iq_file_path
    scale: '16768'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [48, 628.0]
    rotation: 180
    state: enabled
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1216, 684.0]
    rotation: 180
    state: true
- name: satnogs_ogg_encoder_0
  id: satnogs_ogg_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filename: file_path
    quality: '1.0'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [704, 404.0]
    rotation: 0
    state: enabled
- name: satnogs_tcp_rigctl_msg_source_0
  id: satnogs_tcp_rigctl_msg_source
  parameters:
    addr: '"127.0.0.1"'
    affinity: ''
    alias: ''
    comment: ''
    interval: int(1000.0/doppler_correction_per_sec) + 1
    maxoutbuf: '0'
    minoutbuf: '0'
    mode: 'False'
    mtu: '1500'
    port: rigctl_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [216, 268.0]
    rotation: 0
    state: enabled
- name: satnogs_udp_msg_sink_0_0
  id: satnogs_udp_msg_sink
  parameters:
    addr: udp_IP
    affinity: ''
    alias: ''
    comment: ''
    mtu: '1500'
    port: udp_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1024, 740.0]
    rotation: 180
    state: enabled
- name: satnogs_waterfall_sink_0
  id: satnogs_waterfall_sink
  parameters:
    affinity: ''
    alias: ''
    center_freq: rx_freq
    comment: ''
    fft_size: '1024'
    filename: waterfall_file_path
    mode: '1'
    rps: '10'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [48, 436.0]
    rotation: 180
    state: enabled
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"driver=invalid"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [216, 28.0]
    rotation: 0
    state: enabled
- name: soapy_source_0
  id: soapy_source
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: antenna
    ant1: RX2
    args: dev_args
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: soapy_rx_device
    devname: custom
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    manual_gain0: 'False'
    manual_gain1: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    mix_gain1: '10'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: gain
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rfgr_gain: '9'
    samp_rate: samp_rate_rx
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    tia_gain0: '0'
    tia_gain1: '0'
    tuner_gain0: '10'
    tuner_gain1: '10'
    type: fc32
    vga_gain0: '10'
    vga_gain1: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [216, 132.0]
    rotation: 0
    state: true
- name: udp_IP
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"127.0.0.1"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 772.0]
    rotation: 0
    state: enabled
- name: udp_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '16887'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 852.0]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [776, 188.0]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [48, 572.0]
    rotation: 0
    state: true
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/waterfall.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 772.0]
    rotation: 0
    state: enabled

connections:
- [analog_quadrature_demod_cf_0, '0', satnogs_ogg_encoder_0, '0']
- [analog_quadrature_demod_cf_0_0, '0', low_pass_filter_1, '0']
- [dc_blocker_xx_0, '0', root_raised_cosine_filter_0, '0']
- [digital_binary_slicer_fb_0_0, '0', satnogs_frame_decoder_0, '0']
- [digital_clock_recovery_mm_xx_0_0_0, '0', digital_binary_slicer_fb_0_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0_0, '0']
- [low_pass_filter_1, '0', dc_blocker_xx_0, '0']
- [root_raised_cosine_filter_0, '0', digital_clock_recovery_mm_xx_0_0_0, '0']
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0, out, satnogs_json_converter_0, in]
- [satnogs_json_converter_0, out, satnogs_frame_file_sink_0_1_0, frame]
- [satnogs_json_converter_0, out, satnogs_udp_msg_sink_0_0, in]
- [satnogs_tcp_rigctl_msg_source_0, freq, satnogs_doppler_compensation_0, doppler]
- [soapy_source_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', low_pass_filter_0, '0']
- [virtual_source_0, '0', satnogs_iq_sink_0, '0']
- [virtual_source_0, '0', satnogs_waterfall_sink_0, '0']

metadata:
  file_format: 1
